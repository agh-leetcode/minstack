class MinStack  {
    private long min;
    private ArrayList<Long> list;

    /** initialize your data structure here. */
    public MinStack() {
        list = new ArrayList<>();
    }

    public void push(int x) {
        if (list.isEmpty()){
            list.add(0L);
            min=x;
        }else {
            list.add(x-min);
            if (x < min)
                min = x;
        }
    }

    public void pop() {
        if (list.isEmpty())
            return;
        long pop =  list.get(list.size()-1);
        list.remove(list.size()-1);

        if (pop<0)
            min = min - pop;
    }

    public int top() {
        long top = list.get(list.size()-1);
        if (top>0){
            return (int) (top+min);
        }else {
            return (int)(min);
        }
    }

    public int getMin() {
        return (int) min;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */